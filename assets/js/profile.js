let profileContainer = document.querySelector("#profileContainer");
let token = localStorage.getItem("token");
let adminUser = localStorage.getItem("isAdmin");
let addButton = document.querySelector("#userButton");

fetch("https://gentle-falls-52725.herokuapp.com/api/users/details", {
  headers: {
    Authorization: `Bearer ${token}`,
  },
})
  .then((res) => res.json())
  .then((data) => {
    if (adminUser === "true" || adminUser === "null") {
      addButton.innerHTML = null;
    } else {
      addButton.innerHTML = `

    <div class="col-md-2 offset-md-10">
      <a href="./editProfile.html?userId=${data._id}" value="${data._id}" class="btn btn-block btn-light editProfile">Edit Profile</a>
    </div>


  `;
    }

    if (adminUser === "true") {
      profileContainer.innerHTML = `<h1 class="text-center">No Profile Available</h1>`;
    } else if (adminUser === "false") {
      let courseNames = [];

      profileContainer.innerHTML = `
			<div class="col-md-12">
				<section class="jumbotron text-center my-5 userProfile">		
					<h2 class="my-2">${data.firstName} ${data.lastName}</h2>
					<p> mobile number: ${data.mobileNo}</p>
					<div id="enrollmentsContainer">
					<h3>Courses</h3>
					</div>
				</section>
			</div>
		`;
      let enrollmentCard = document.querySelector("#enrollmentsContainer");

      data.enrollments.map((enrollment) => {
        fetch(
          `https://gentle-falls-52725.herokuapp.com/api/courses/${enrollment.courseId}`
        )
          .then((res) => res.json())
          .then((data) => {
            enrollmentCard.innerHTML += `
					<div class="card">
						<div class="card-body">
						    <h5 class="card-title">${data.name}</h5>
						    <h6 class="card-subtitle mb-2 text-muted">Enrolled On: ${Date(
                  enrollment.enrolledOn
                ).toLocaleString()}</h6>
						    <h6 class="card-subtitle mb-2 text-muted">Status:${enrollment.status}</h6>
						</div>
					</div>

				`;
          });
      });
    }
  });
