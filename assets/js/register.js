let registerForm = document.querySelector("#registerUser");

// addEventListener("event", function)
// 	allows us to add a specific event into an element.This event can trigger a function for our page to do

// Submit event - allows us to submit a form.
// It's default behavior is that it sends your form and refreshes the page
registerForm.addEventListener("submit", (e) => {
  // e = event object. This event object peratins to the event and where it was triggered
  // preventDefault() = prevents the submit event of its default behavior

  e.preventDefault();

  console.log("I triggered the submit event");

  let firstName = document.querySelector("#firstName").value;
  let lastName = document.querySelector("#lastName").value;
  let mobileNo = document.querySelector("#mobileNumber").value;
  let email = document.querySelector("#userEmail").value;
  let password1 = document.querySelector("#password1").value;
  let password2 = document.querySelector("#password2").value;

  // console.log(firstName)
  // console.log(lastName);
  // console.log(mobileNo);
  // console.log(email);
  // console.log(password1);
  // console.log(password2);
  if (
    password1 !== "" &&
    password2 !== "" &&
    password1 === password2 &&
    mobileNo.length === 11
  ) {
    fetch("https://gentle-falls-52725.herokuapp.com/api/users/email-exists", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        email: email,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (data === false) {
          fetch("https://gentle-falls-52725.herokuapp.com/api/users", {
            method: "POST",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify({
              firstName: firstName,
              lastName: lastName,
              email: email,
              mobileNo: mobileNo,
              password: password1,
            }),
          })
            .then((res) => res.json())
            .then((data) => {
              console.log(data);

              if (data === true) {
                alert("registered successfully");
                // redirect to login page
                window.location.replace("./login.html");
              } else {
                // error in creating registration
                alert("something went wrong");
              }
            });
        }
      });
  }
});
