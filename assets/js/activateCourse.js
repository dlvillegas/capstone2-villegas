let params = new URLSearchParams(window.location.search);
let courseId = params.get("courseId");
let token = localStorage.getItem("token");

fetch(
  `https://gentle-falls-52725.herokuapp.com/api/courses/activate/${courseId}`,
  {
    method: "PUT",
    headers: {
      Authorization: `bearer ${token}`,
    },
  }
)
  .then((res) => res.json())
  .then((data) => {
    console.log(data);

    if (data === true) {
      window.location.replace("./courses.html");
    } else {
      alert("something went wrong");
    }
  });
