

	let navItems = document.querySelector("#navSession");
	let registerLink = document.querySelector('#register')

	let userToken = localStorage.getItem("token");
	let profileItems = document.querySelector("#profile")
	let adminUser1 = localStorage.getItem("isAdmin") === "true"
	if(!userToken) {
		navItems.innerHTML = 
		`
			<li class="nav-item ">
				<a href="./login.html" class="nav-link"> Login </a>
			</li>
		`
		registerLink.innerHTML = 
		`
			<li class="nav-item ">
				<a href="./register.html" class="nav-link"> Register </a>
			</li>
		`
	} else if (adminUser1 === false) {
		profileItems.innerHTML = 
		`
			<li class="nav-item ">
				<a href="./profile.html" class="nav-link"> Profile </a>
			</li>
		`
	} 
	
	if (userToken) {
		navItems.innerHTML = 
		`
			<li class="nav-item ">
				<a href="./logout.html" class="nav-link"> Log Out </a>
			</li>
		`
	}