let token = localStorage.getItem("token");
let adminUser = localStorage.getItem("isAdmin") === "true";
let addButton = document.querySelector("#adminButton");
let cardFooter;

//This if statement will allow us to show a button for adding a course;a button to redirect us to the addCourse page if the user is admin, however, if he/she is a guest or a regular user, they should not see a button.
if (adminUser === false || adminUser === null) {
  addButton.innerHTML = null;
} else {
  addButton.innerHTML = `

		<div class="col-md-2 offset-md-10">
			<a href="./addCourse.html" class="btn btn-block btn-light addCourse">Add Course</a>
		</div>


	`;
}

fetch("https://gentle-falls-52725.herokuapp.com/api/courses", {
  headers: {
    Authorization: `Bearer ${token}`,
  },
})
  .then((res) => res.json())
  .then((data) => {
    console.log(data);

    // A variable that will store the data to be rendered
    let courseData;

    // if the number of courses is less than 1, display no courses available
    if (data.length < 1) {
      courseData = "No courses available";
    } else {
      courseData = data
        .map((course) => {
          let button;
          if (course.isActive == true) {
            button = `
						<a href="./deleteCourse.html?courseId=${course._id}" value={course._id} class="btn btn-danger btn-block deleteButton">
						Archive Course
					</a>
						`;
          } else {
            button = `
						<a href="./activateCourse.html?courseId=${course._id}" value={course._id} class="btn btn-success btn-block deleteButton">
						Activate Course
						</a>
						`;
          }
          console.log(course);
          if (adminUser == false || !adminUser || adminUser === null) {
            cardFooter = `
						<a href="./course.html?courseId=${course._id}" value={course._id} class="btn btn-block btn-dark selectButton">
							Select Course
						</a>
					`;
          } else {
            cardFooter = `
						<a href="./editCourse.html?courseId=${course._id}" value={course._id} class="btn btn-success btn-block editButton">
							Edit
						</a>
						<a href="./course.html?courseId=${course._id}" value={course._id} class="btn btn-primary btn-block deleteButton">
							Go to Course
						</a>
						${button}
					`;
          }

          return `
				<div class="col-md-6 my-3">
					<div class="card course-card">
						<div class="card-body">
							<h5 class="card-title">${course.name}</h5>
							<p class="card-text text-left">
							${course.description}
							</p>
							<p class="card-text text-right">
							₱ ${course.price}
							</p>
						</div>
						<div class="card-footer">
						${cardFooter}
						</div>
					</div>
				</div>
				`;
          // since the collection is an array, we can use the join method to indicate the separator for each element
        })
        .join("");
    }

    let container = document.querySelector("#coursesContainer");

    container.innerHTML = courseData;
  });

/*
	Activity:

	use the fetch Method to get all the courses and show the data into the console using console.log()

	If you are done, create a new folder (s19) in gitlab
	inside s19 folder, create a new repo: d1

	In your local machine, connect your d1 to your online repo:
	git remote add origin <url>

	Then add, commit and push it into your new repo.

	Link to boodle as:
	WD057-11 | Express.js - Booking System API Integration


*/
