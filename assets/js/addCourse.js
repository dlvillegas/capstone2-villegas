let formSubmit = document.querySelector("#createCourse");

// add an event lister:
formSubmit.addEventListener("submit", (e) => {
  e.preventDefault();

  // get the values of your input:
  let courseName = document.querySelector("#courseName").value;
  let description = document.querySelector("#courseDescription").value;
  let price = document.querySelector("#coursePrice").value;

  // get the JWT from our localStorage
  let token = localStorage.getItem("token");
  console.log(token);

  // create a fetch request to add a new course
  fetch("https://gentle-falls-52725.herokuapp.com/api/courses", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
    },
    body: JSON.stringify({
      name: courseName,
      description: description,
      price: price,
    }),
  })
    .then((res) => res.json())
    .then((data) => {
      console.log(data);
      // if the creation of the course is successful, redirect admin to the courses page
      if (data === true) {
        // redirect admin to the courses page
        window.location.replace("./courses.html");
      } else {
        // Error while creating a course:
        alert("Course Creation Failed. Something Went Wrong.");
      }
    });
});
